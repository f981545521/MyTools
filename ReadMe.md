## IDEA PLUGIN

### 开发文档
- [HelloWorld](https://www.jianshu.com/p/91d0bdbbe79f)

- [IDEA 插件文档](https://plugins.jetbrains.com/docs/intellij/welcome.html)

- [tool-windows 帮助文档](https://plugins.jetbrains.com/docs/intellij/tool-windows.html?from=jetbrains.org#how-to-create-a-tool-window)

- [IntelliJ Platform UI Guidelines](https://jetbrains.design/intellij/)

- [UI开发](https://plugins.jetbrains.com/docs/intellij/user-interface-components.html)

- [图标列表](https://jetbrains.design/intellij/resources/icons_list/)

### 部分参考自
- [MyPojoToJson](https://github.com/luchuanbaker/MyPojoToJson/)

- [CamelCase](https://plugins.jetbrains.com/plugin/7160-camelcase)


### 如何安装
1. 从[发行版](https://gitee.com/f981545521/MyTools/releases)中，下载最新的插件包。
2. Settings -> plugins -> install Plugin from disk.
3. 选择文件即可

### 如何使用

#### 1. 将实体转换为JOSN
方法1：

![](documnet/images/convertJson_1.png)

方法2：

![](documnet/images/convertJson_2.png)
#### 2. 从JOSN中创建实体
步骤1：

![](documnet/images/createEntity_1.png)

步骤2：

![](documnet/images/createEntity_2.png)
#### 3. 一键生成serialVersionUID
当实体继承`Serializable`接口，未生成`serialVersionUID`时，可以使用：

![](documnet/images/serialVersion_1.png)

#### 4. 字段各种CamelCase转换

1. 参考自：[CamelCase](https://plugins.jetbrains.com/plugin/7160-camelcase)
2. 选中字段，使用快捷键`shift+alt+U`转换。

#### 5. URLEncode/URLDecode 与 Base64Encode/Base64Decode

1. 使用面板按钮操作即可。

![](documnet/images/encode_tools.png)


1. 参考自：[CamelCase](https://plugins.jetbrains.com/plugin/7160-camelcase)
2. 选中字段，使用快捷键`shift+alt+U`转换。


#### 6. YML与Properties转换

![](documnet/images/ymlConvert1.png)

![](documnet/images/ymlConvert2.png)

#### 7. 复制Mapping

![](documnet/images/copyMapping.png)


#### 8. 生成所有Setter方法（支持Lombok）

![](documnet/images/allSetter.png)

### 项目运行

IDEA新版需要使用JDK11来编译！！

使用 `Gradle`， 与`Task: :runIde`

![](documnet/images/how_run.png)
### 打包
版本号：`build.gradle -> version '1.1'`

如何打包：`Gradle面板->intellij->buildPlugin`

文件目录：`build/libs/MyTools-x.x.jar`