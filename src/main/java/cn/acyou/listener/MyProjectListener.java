package cn.acyou.listener;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManagerListener;
import org.jetbrains.annotations.NotNull;

/**
 * @author youfang
 * @version [1.0.0, 2022/1/28 10:02]
 **/
public class MyProjectListener implements ProjectManagerListener {

    @Override
    public void projectOpened(@NotNull Project project) {
        //System.out.println("打开项目：" + project.getName());
    }
}
