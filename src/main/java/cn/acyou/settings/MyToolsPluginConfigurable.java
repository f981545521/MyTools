package cn.acyou.settings;

import cn.acyou.actions.camelCase.CamelCaseConfig;
import cn.acyou.actions.camelCase.OptionGui;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class MyToolsPluginConfigurable implements SearchableConfigurable {

    private MyToolsConfigGui mGUI;
    private final MyToolsConfig mConfig;

    public MyToolsPluginConfigurable() {
        mConfig = MyToolsConfig.getInstance();
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        mGUI = new MyToolsConfigGui();
        mGUI.createUI();
        return mGUI.getRootPanel();
    }

    @Override
    public void disposeUIResources() {
        mGUI = null;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "MyTools";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return "preference.MyToolsPluginConfigurable";
    }

    @NotNull
    @Override
    public String getId() {
        return "preference.MyToolsPluginConfigurable";
    }

    @Nullable
    @Override
    public Runnable enableSearch(String s) {
        return null;
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() throws ConfigurationException {
        mGUI.apply();
    }

    @Override
    public void reset() {

    }

}