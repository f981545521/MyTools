package cn.acyou.settings;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(
        name = "MyToolsConfig",
        storages = {
                @Storage("MyToolsConfig.xml")}
)
public class MyToolsConfig implements PersistentStateComponent<MyToolsConfig> {
    public String trans_APP_ID = "";
    public String trans_SECURITY_KEY = "";
    public boolean useSerialVersionIdRandom = false;


    public String getTrans_APP_ID() {
        return trans_APP_ID;
    }

    public void setTrans_APP_ID(String trans_APP_ID) {
        this.trans_APP_ID = trans_APP_ID;
    }

    public String getTrans_SECURITY_KEY() {
        return trans_SECURITY_KEY;
    }

    public void setTrans_SECURITY_KEY(String trans_SECURITY_KEY) {
        this.trans_SECURITY_KEY = trans_SECURITY_KEY;
    }


    public boolean isUseSerialVersionIdRandom() {
        return useSerialVersionIdRandom;
    }

    public void setUseSerialVersionIdRandom(boolean useSerialVersionIdRandom) {
        this.useSerialVersionIdRandom = useSerialVersionIdRandom;
    }

    @NotNull
    public static MyToolsConfig getInstance() {
        return ServiceManager.getService(MyToolsConfig.class);
    }

    @Nullable
    @Override
    public MyToolsConfig getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull MyToolsConfig config) {
        XmlSerializerUtil.copyBean(config, this);
    }
}