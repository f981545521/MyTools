package cn.acyou.settings;

import cn.acyou.utils.CommonCache;
import cn.acyou.utils.CommonUtil;
import cn.acyou.utils.MessageUtil;
import com.alibaba.fastjson.JSON;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.util.Icons;

import javax.swing.*;
import java.util.Map;

/**
 * @author youfang
 * @version [1.0.0, 2021/4/1]
 **/
public class MyToolsConfigGui {
    private JTextField appId;
    private JTextField appSerect;
    private JPanel parentPanel;
    private JLabel Serial_Version_Random;
    private JCheckBox servialVersionCheckBox;
    private JButton clearCache;
    private JButton viewCacheButton;
    private JButton viewLogButton;
    private MyToolsConfig mConfig;

    public void createUI() {
        mConfig = MyToolsConfig.getInstance();
        appId.setText(mConfig.getTrans_APP_ID());
        appSerect.setText(mConfig.getTrans_SECURITY_KEY());
        servialVersionCheckBox.setSelected(mConfig.isUseSerialVersionIdRandom());
        clearCache.setText("清除缓存（" + CommonCache.getItemCount() + "）");
        clearCache.addActionListener(e -> {
            CommonCache.clearAllCache();
            clearCache.setText("清除缓存成功");
        });
        viewCacheButton.addActionListener(e -> {
            StringBuilder sb = new StringBuilder();
            sb.append("——————————————————————————————————1 obj" + CommonUtil.lineSeparator);
            for (Map.Entry<String, Object> entry : CommonCache.OBJ_CACHE_MAP.entrySet()) {
                sb.append(entry.getKey() + ":" + CommonUtil.lineSeparator);
                sb.append(JSON.toJSONString(entry.getValue()) + CommonUtil.lineSeparator);
            }
            sb.append("——————————————————————————————————2 str" + CommonUtil.lineSeparator);
            for (Map.Entry<String, String> entry : CommonCache.CACHE_MAP.entrySet()) {
                sb.append(entry.getKey() + ":" + CommonUtil.lineSeparator);
                sb.append(entry.getValue() + CommonUtil.lineSeparator);
            }
            sb.append("——————————————————————————————————end" + CommonUtil.lineSeparator);
            Messages.showMessageDialog(sb.toString(), "查看缓存", null);
        });
        viewLogButton.addActionListener(e -> {
            StringBuilder sb = new StringBuilder();
            for (String s : CommonCache.LOG) {
                sb.append(s + CommonUtil.lineSeparator);
            }
            Messages.showMessageDialog(sb.toString(), "查看日志", null);
        });
    }

    public JPanel getRootPanel() {
        return parentPanel;
    }

    public void apply() {
        mConfig.setTrans_APP_ID(appId.getText());
        mConfig.setTrans_SECURITY_KEY(appSerect.getText());
        mConfig.setUseSerialVersionIdRandom(servialVersionCheckBox.isSelected());
    }
}
