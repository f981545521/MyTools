package cn.acyou.actions.resourcesTool;

import cn.acyou.utils.CommonUtil;
import cn.acyou.utils.MessageUtil;
import com.intellij.ide.projectView.ProjectView;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * 生成序列化版本号操作
 *
 * @author youfang
 * @date 2021/04/01
 */
public class GenerateYmlPropertiesAction extends AnAction {

    @Override
    public void update(AnActionEvent e) {
        boolean enableIt = false;
        e.getPresentation().setText("---");
        VirtualFile[] virtualFiles = e.getData(PlatformDataKeys.VIRTUAL_FILE_ARRAY);
        if(virtualFiles != null && virtualFiles.length == 1){
            if(!virtualFiles[0].isDirectory()){
                String extension = virtualFiles[0].getExtension();
                if ("yml".equalsIgnoreCase(extension) ) {
                    e.getPresentation().setText("将yml复制为properties");
                    enableIt = true;
                }
                if ("properties".equalsIgnoreCase(extension) ) {
                    e.getPresentation().setText("将properties复制为yml");
                    enableIt = true;
                }
            }
        }
        e.getPresentation().setEnabledAndVisible(enableIt);
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        final VirtualFile actionFile = e.getData(LangDataKeys.VIRTUAL_FILE);
        final String parentPath = actionFile.getParent().getPath();
        final String nameWithoutExtension = actionFile.getNameWithoutExtension();
        final String extension = actionFile.getExtension();
        final Project project = e.getProject();
        ProgressManager.getInstance().run(new Task.Backgroundable(project, "Converting ...") {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                // 10% done
                indicator.setFraction(0.1);
                indicator.setText("90% to finish.");
                indicator.setIndeterminate(false);

                try {
                    String realName = "";
                    String content = "";
                    if ("properties".equalsIgnoreCase(extension)) {
                        realName = nameWithoutExtension + "." + "yml";
                        content = CommonUtil.propertiesToYml(actionFile.getInputStream());
                    }
                    if ("yml".equalsIgnoreCase(extension)) {
                        realName = nameWithoutExtension + "." + "properties";
                        content = CommonUtil.ymlToProperties(actionFile.getInputStream());
                    }
                    if (realName.isEmpty()) {
                        return;
                    }
                    // 50% done
                    indicator.setFraction(0.5);
                    indicator.setText("50% to finish.");

                    File directory = new File(parentPath + File.separator + realName);
                    FileUtil.writeToFile(directory, content);

                    // Finished
                    indicator.setFraction(1.0);
                    indicator.setText("Finished");
                    indicator.cancel();
                } catch (Exception ex) {
                    //ignore
                }

                String message = "转换成功！";
                MessageUtil.notifyProject(message, project);
                // Refresh UI
                try {
                    Thread.sleep(10);
                    ProjectView.getInstance(project).refresh();
                    actionFile.getParent().refresh(false, true);
                } catch (InterruptedException ignored) {
                    //ignore
                }
            }
        });



    }



}
