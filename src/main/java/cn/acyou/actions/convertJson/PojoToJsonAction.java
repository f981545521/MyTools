package cn.acyou.actions.convertJson;

import cn.acyou.exception.UnknownException;
import cn.acyou.utils.MessageUtil;
import cn.acyou.utils.PojoToJsonCore;
import cn.acyou.utils.ProcessingInfo;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.progress.ProcessCanceledException;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiType;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;

import static cn.acyou.utils.PojoToJsonCore.GSON;

/**
 * pojo 转 json操作
 *
 * @author pc22
 * @date 2021/04/01
 */
public class PojoToJsonAction extends AnAction {

    @Override
    public void update(AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        PsiType psiType = PojoToJsonCore.checkAndGetPsiType(dataContext);
        boolean enableIt = psiType != null;
        e.getPresentation().setEnabledAndVisible(enableIt);
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        Project project = e.getProject();
        if (project == null) {
            return;
        }

        DataContext dataContext = e.getDataContext();

        PsiClassType psiType = PojoToJsonCore.checkAndGetPsiType(dataContext);
        if (psiType == null) {
            return;
        }

        String className = PojoToJsonCore.getClassName(psiType);

        ProgressManager.getInstance().run(new Task.Backgroundable(project, "Converting " + className + " to JSON...") {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                // 10% done
                indicator.setFraction(0.1);
                indicator.setText("90% to finish");
                ProcessingInfo processingInfo = new ProcessingInfo().setProject(project).setProgressIndicator(indicator);
                try {
                    ApplicationManager.getApplication().runReadAction(new Runnable() {
                        @Override
                        public void run() {
                            Object result = PojoToJsonCore.resolveType(psiType, processingInfo);
                            // fix simpleType not support
                            processingInfo.setResultIfAbsent(result);
                        }
                    });
                } catch (ProcessCanceledException e) {
                    // ignore
                } finally {
                    // Finished
                    indicator.setFraction(1.0);
                    indicator.setText("finished");
                    indicator.cancel();
                }

                Object result = processingInfo.getResult();
                if (result == null) {
                    return;
                }

                String json = GSON.toJson(result);
                try {
                    json = PojoToJsonCore.myFormat(json);
                } catch (IOException ex) {
                    throw new UnknownException("Error :" +  ex.getMessage());
                }
                StringSelection selection = new StringSelection(json);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
                String message = "转换 " + className + " 为 JSON 成功，已复制到剪切板！";
                MessageUtil.notifyProject(message, project);
            }
        });

    }

}
