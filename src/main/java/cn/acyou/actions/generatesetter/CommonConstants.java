package cn.acyou.actions.generatesetter;

public class CommonConstants {

    public static final String GENERATE_SETTER_METHOD = "生成所有setter(有默认值)";
    public static final String GENERATE_SETTER_METHOD_NO_DEAULT_VALUE = "生成所有setter(无默认值)";
    public static final String BUILDER_METHOD_NAME = "builder";
}
