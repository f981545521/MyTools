package cn.acyou.actions.generatesetter.actions;

import cn.acyou.actions.generatesetter.CommonConstants;
import cn.acyou.actions.generatesetter.GenerateAllHandlerAdapter;
import org.jetbrains.annotations.NotNull;

/**
 * Created by bruce.ge on 2016/12/23.
 */
public class GenerateAllSetterAction extends GenerateAllSetterBase {


    public GenerateAllSetterAction() {
        super(new GenerateAllHandlerAdapter() {
            @Override
            public boolean shouldAddDefaultValue() {
                return true;
            }
        });
    }

    @NotNull
    @Override
    public String getText() {
        return CommonConstants.GENERATE_SETTER_METHOD;
    }
}
