package cn.acyou.actions.generatesetter.actions;


import cn.acyou.actions.generatesetter.CommonConstants;
import cn.acyou.actions.generatesetter.GenerateAllHandlerAdapter;

/**
 * @author bruce ge
 */
public class GenerateAllSetterNoDefaultValueAction extends GenerateAllSetterBase {
    public GenerateAllSetterNoDefaultValueAction() {
        super(new GenerateAllHandlerAdapter() {
            @Override
            public boolean shouldAddDefaultValue() {
                return false;
            }
        });
    }

    @Override
    public String getText() {
        return CommonConstants.GENERATE_SETTER_METHOD_NO_DEAULT_VALUE;
    }
}
