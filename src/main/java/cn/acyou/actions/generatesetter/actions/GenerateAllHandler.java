package cn.acyou.actions.generatesetter.actions;

/**
 * @author bruce ge
 */
public interface GenerateAllHandler {
    public boolean shouldAddDefaultValue();

    boolean isSetter();

    boolean isFromMethod();

    String formatLine(String line);

    boolean forBuilder();
}
