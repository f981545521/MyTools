package cn.acyou.actions.createEntity;

import javax.swing.*;

/**
 * @author youfang
 * @version [1.0.0, 2021/3/26]
 **/
public class CreateEntityClass {
    private JPanel parentContent;
    private JTextField fileName;
    private JTextArea fileContent;
    private JLabel fileNameLabel;

    public JPanel getContent() {
        return parentContent;
    }

    public String getFileName() {
        return fileName.getText();
    }

    public String getFileContent() {
        return fileContent.getText();
    }
}
