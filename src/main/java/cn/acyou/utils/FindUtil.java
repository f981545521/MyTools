package cn.acyou.utils;

import com.intellij.find.FindManager;
import com.intellij.find.FindModel;
import com.intellij.find.impl.FindInProjectUtil;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.PsiShortNamesCache;
import com.intellij.usageView.UsageInfo;
import com.intellij.util.CommonProcessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author youfang
 * @version [1.0.0, 2021/12/30 13:44]
 **/
public class FindUtil {

    public static List<UsageInfo> findFileInProject(Project project, String stringToFind) {
        List<UsageInfo> usages = new ArrayList<>();
        FindManager findManager = FindManager.getInstance(project);
        FindModel findInFileModel = findManager.getFindInFileModel();
        findInFileModel.setCaseSensitive(true);
        findInFileModel.setCustomScope(false);
        findInFileModel.setStringToFind(stringToFind);
        findInFileModel.setProjectScope(true);
        FindInProjectUtil.findUsages(findInFileModel, project,
                new CommonProcessors.CollectProcessor<>(
                        usages), FindInProjectUtil
                        .setupProcessPresentation(project, false, FindInProjectUtil.setupViewPresentation(false, findInFileModel)));
        return usages;
    }

    public static PsiFile[] findProjectFileByName(Project project, String fileName){
        return PsiShortNamesCache.getInstance(project).getFilesByName(fileName);
    }
}
