package cn.acyou.utils;

import cn.acyou.actions.createEntity.CreateEntityClass;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

/**
 * @author youfang
 * @version [1.0.0, 2021/3/25]
 **/
public class SampleDialogWrapper extends DialogWrapper {
    CreateEntityClass createEntityClass = new CreateEntityClass();

    public SampleDialogWrapper(String title) {
        super(true);
        init();
        setTitle(title);
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {

        JPanel dialogPanel = new JPanel(new BorderLayout());
        dialogPanel.setPreferredSize(new Dimension(500, 400));
        dialogPanel.add(createEntityClass.getContent());
        return dialogPanel;
    }

    public String getFileName() {
        return createEntityClass.getFileName();
    }

    public String getFileContent() {
        return createEntityClass.getFileContent();
    }
}