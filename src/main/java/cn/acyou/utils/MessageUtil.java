package cn.acyou.utils;

import com.intellij.notification.*;
import com.intellij.openapi.project.Project;

/**
 * @author youfang
 * @version [1.0.0, 2021/3/24]
 **/
public class MessageUtil {
    public static final NotificationGroup notifyGroup = new NotificationGroup("Tools.NotificationGroup", NotificationDisplayType.BALLOON, true);

    /**
     * 通知项目
     *
     * @param message 消息
     * @param project 项目
     */
    public static void notifyProject(String message, Project project) {
        Notification success = MessageUtil.notifyGroup.createNotification(message, NotificationType.INFORMATION);
        Notifications.Bus.notify(success, project);
    }
}
