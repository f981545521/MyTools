package cn.acyou.utils;

import com.intellij.openapi.util.PropertiesUtil;
import org.yaml.snakeyaml.Yaml;

/**
 * @author youfang
 * @version [1.0.0, 2022-1-17]
 **/
public class MainTest {
    public static void main(String[] args) {
        String text = "server:\n" +
                "  port: 8056\n" +
                "spring:\n" +
                "  application:\n" +
                "    name: product-service\n" +
                "  profiles:\n" +
                "    active: '*'";
        final Object load = new Yaml().load(text);
        System.out.println(load);
    }


    public static void mai2n(String[] args) {
        String text = "server:\n" +
                "  port: 8056\n" +
                "spring:\n" +
                "  application:\n" +
                "    name: product-service\n" +
                "  profiles:\n" +
                "    active: dev";
        final Object load = new Yaml().load(text);
        System.out.println(load);
    }
}
