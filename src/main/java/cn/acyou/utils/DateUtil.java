package cn.acyou.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author youfang
 * @version [1.0.0, 2021/3/26]
 **/
public class DateUtil {
    public static final String SHORT_DATE_FORMAT_PATTERN = "yyyyMMdd";
    public static final String DEFAULT_DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    public static final String DEFAULT_TIME_FORMAT_PATTERN = "HH:mm:ss";
    public static final String SPECIFIC_DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_DAY_MIN_TIME = " 00:00:00";
    public static final String DATE_DAY_MAX_TIME = " 23:59:59";

    public static String getNowSpecificString(){
        return new SimpleDateFormat(SPECIFIC_DATE_TIME_FORMAT_PATTERN).format(new Date());
    }

    public static void main(String[] args) {
        System.out.println(getNowSpecificString());
    }
}
