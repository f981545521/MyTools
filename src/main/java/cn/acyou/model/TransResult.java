package cn.acyou.model;

import java.io.Serializable;
import java.util.List;


/**
 * 百度翻译 TransResult
 * @version [1.0.0, 2021-03-26 20:39:34] 
 */ 
public class TransResult implements Serializable {

    private static final long serialVersionUID = 1854011372133444291L;

    private String from;

    private String to;

    private List<TransR> trans_result;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<TransR> getTrans_result() {
        return trans_result;
    }

    public void setTrans_result(List<TransR> trans_result) {
        this.trans_result = trans_result;
    }

    class TransR {
        private String src;
        private String dst;

        public String getSrc() {
            return src;
        }

        public void setSrc(String src) {
            this.src = src;
        }

        public String getDst() {
            return dst;
        }

        public void setDst(String dst) {
            this.dst = dst;
        }
    }

}